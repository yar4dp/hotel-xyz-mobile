package id.faizal.hotelxyz.feature.facility

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import id.faizal.hotelxyz.R
import id.faizal.hotelxyz.feature.detail.DetailActivity
import id.faizal.hotelxyz.model.DetailModel
import id.faizal.hotelxyz.model.FacilityModel
import id.faizal.hotelxyz.model.ResponseModel
import id.faizal.hotelxyz.network.apiService
import id.faizal.hotelxyz.utilities.showAlertDialog
import kotlinx.android.synthetic.main.activity_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FacilityActivity : AppCompatActivity(), Callback<ResponseModel<List<FacilityModel>>> {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        title = "Facility"

        recyclerViewLayout.visibility = View.GONE

        apiService.listFacility().enqueue(this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResponse(call: Call<ResponseModel<List<FacilityModel>>>?, response: Response<ResponseModel<List<FacilityModel>>>) {
        if (response.body()?.status == "200") {
            showFacilityList(response.body()!!.data)
        }
    }

    override fun onFailure(call: Call<ResponseModel<List<FacilityModel>>>?, t: Throwable) {
        showAlertDialog(this, "Failed", t.localizedMessage)
    }

    fun showFacilityList(list: List<FacilityModel>) {
        progressBarLayout.visibility = View.GONE
        recyclerViewLayout.visibility = View.VISIBLE
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = FacilityAdapter(this, list)
                .setItemClickListener(object : FacilityAdapter.ItemClickListener {
                    override fun onItemClicked(item: FacilityModel) {
                        showDetail(DetailModel.fromFacility(item))
                    }
                })
        recyclerView.isNestedScrollingEnabled = false
    }

    fun showDetail(detail: DetailModel) {
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra("detail", detail)
        startActivity(Intent(intent))
    }

}
