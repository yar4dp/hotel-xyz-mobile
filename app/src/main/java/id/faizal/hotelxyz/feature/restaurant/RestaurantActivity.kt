package id.faizal.hotelxyz.feature.restaurant

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.view.MenuItem
import android.view.View
import id.faizal.hotelxyz.R
import id.faizal.hotelxyz.feature.detail.DetailActivity
import id.faizal.hotelxyz.model.DetailModel
import id.faizal.hotelxyz.model.ResponseModel
import id.faizal.hotelxyz.model.RestaurantModel
import id.faizal.hotelxyz.network.apiService
import id.faizal.hotelxyz.utilities.showAlertDialog
import kotlinx.android.synthetic.main.activity_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RestaurantActivity : AppCompatActivity(), Callback<ResponseModel<List<RestaurantModel>>> {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        title = "Restaurant"

        recyclerViewLayout.visibility = View.GONE

        apiService.listRestaurant().enqueue(this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResponse(call: Call<ResponseModel<List<RestaurantModel>>>?, response: Response<ResponseModel<List<RestaurantModel>>>) {
        if (response.body()?.status == "200") {
            showRestaurantList(response.body()!!.data)
        }
    }

    override fun onFailure(call: Call<ResponseModel<List<RestaurantModel>>>?, t: Throwable) {
        showAlertDialog(this, "Failed", t.localizedMessage)
    }

    fun showRestaurantList(list: List<RestaurantModel>) {
        progressBarLayout.visibility = View.GONE
        recyclerViewLayout.visibility = View.VISIBLE
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = GridLayoutManager(this, 2)
        recyclerView.adapter = RestaurantAdapter(this, list)
                .setItemClickListener(object : RestaurantAdapter.ItemClickListener {
                    override fun onItemClicked(item: RestaurantModel) {
                        showDetail(DetailModel.fromRestaurant(item))
                    }
                })
        recyclerView.isNestedScrollingEnabled = false
    }

    fun showDetail(detail: DetailModel) {
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra("detail", detail)
        startActivity(Intent(intent))
    }
}
