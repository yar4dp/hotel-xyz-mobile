package id.faizal.hotelxyz.feature.room

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import id.faizal.hotelxyz.R
import id.faizal.hotelxyz.feature.detail.DetailActivity
import id.faizal.hotelxyz.model.DetailModel
import id.faizal.hotelxyz.model.ResponseModel
import id.faizal.hotelxyz.model.RoomModel
import id.faizal.hotelxyz.network.apiService
import id.faizal.hotelxyz.utilities.showAlertDialog
import kotlinx.android.synthetic.main.activity_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RoomActivity : AppCompatActivity(), Callback<ResponseModel<List<RoomModel>>> {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        title = "Room"

        recyclerViewLayout.visibility = View.GONE

        apiService.listRoom().enqueue(this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResponse(call: Call<ResponseModel<List<RoomModel>>>?, response: Response<ResponseModel<List<RoomModel>>>) {
        if (response.body()?.status == "200") {
            showRoomList(response.body()!!.data)
        }
    }

    override fun onFailure(call: Call<ResponseModel<List<RoomModel>>>?, t: Throwable) {
        showAlertDialog(this, "Failed", t.localizedMessage)
    }

    fun showRoomList(list: List<RoomModel>) {
        progressBarLayout.visibility = View.GONE
        recyclerViewLayout.visibility = View.VISIBLE
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = RoomAdapter(this, list)
                .setItemClickListener(object : RoomAdapter.ItemClickListener {
                    override fun onItemClicked(item: RoomModel) {
                        showDetail(DetailModel.fromRoom(item), item)
                    }
                })
        recyclerView.isNestedScrollingEnabled = false
    }

    fun showDetail(detail: DetailModel, room: RoomModel) {
        val intent = Intent(this, DetailActivity::class.java)
        intent.putExtra("detail", detail)
        intent.putExtra("room", room)
        startActivity(intent)
    }

}
