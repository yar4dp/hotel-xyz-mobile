package id.faizal.hotelxyz.feature.authenticate.login

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import id.faizal.hotelxyz.R
import id.faizal.hotelxyz.feature.authenticate.register.RegisterActivity
import id.faizal.hotelxyz.model.ResponseModel
import id.faizal.hotelxyz.model.TokenModel
import id.faizal.hotelxyz.network.apiService
import id.faizal.hotelxyz.utilities.Customer
import id.faizal.hotelxyz.utilities.showDashboard
import id.faizal.hotelxyz.utilities.showSnackbar
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivity : AppCompatActivity(), Callback<ResponseModel<TokenModel>>, Customer.GetProfileInterface {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        registerButton.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
        }

        loginButton.setOnClickListener {
            if (loginValid()) {
                login()
            }
        }
    }

    private fun loginValid() : Boolean {
        if (inputEmail.text.toString().isEmpty()) {
            showSnackbar(window.decorView, "Please input your email.")
            return false
        }
        if (inputPassword.text.toString().isEmpty()) {
            showSnackbar(window.decorView, "Please input your password.")
            return false
        }
        return true
    }

    private fun login() {
        apiService.customerLogin(
                inputEmail.text.toString(),
                inputPassword.text.toString()
        ).enqueue(this)
    }

    override fun onResponse(call: Call<ResponseModel<TokenModel>>?, response: Response<ResponseModel<TokenModel>>) {
        if (response.body()?.status == "200") {
            Customer.login(this, response.body()!!.data, this)
        } else {
            showSnackbar(window.decorView, response.body()!!.message)
        }
    }

    override fun onFailure(call: Call<ResponseModel<TokenModel>>?, t: Throwable) {
        showSnackbar(window.decorView, t.localizedMessage)
    }

    private fun loginSuccess() {
        if (intent.extras != null) {
            val previousIntent = intent.extras["previousIntent"] as Intent?
            previousIntent?.putExtra("message", "Welcome back!")
            startActivity(previousIntent)
            finish()
        } else {
            showDashboard(this)
            finishAffinity()
        }
    }

    override fun profileSuccessRetrieved() {
        loginSuccess()
    }

    override fun profileFailedToRetrievedBecause(message: String) {
        showSnackbar(window.decorView, message)
    }

}
