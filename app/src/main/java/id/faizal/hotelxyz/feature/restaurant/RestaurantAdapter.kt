package id.faizal.hotelxyz.feature.restaurant

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import id.faizal.hotelxyz.R
import id.faizal.hotelxyz.model.RestaurantModel

class RestaurantAdapter(
        val context: Context,
        val list: List<RestaurantModel>
) : RecyclerView.Adapter<RestaurantAdapter.ViewHolder>() {

    var clickListener: ItemClickListener? = null

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.showRestaurantItem(list[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.count()
    }

    inner open class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        var item: RestaurantModel? = null

        override fun onClick(p0: View?) {
            clickListener?.onItemClicked(item!!)
        }

        open fun showRestaurantItem(item: RestaurantModel) {
            this.item = item
            val imageView = itemView.findViewById<ImageView>(R.id.imageView)
            val textView = itemView.findViewById<TextView>(R.id.textView)
            textView.text = item.name
            Picasso.with(itemView.context).load(item.imageUrl).into(imageView)
            itemView.setOnClickListener(this)
        }

    }

    fun setItemClickListener(clickListener: ItemClickListener): RestaurantAdapter {
        this.clickListener = clickListener
        return this
    }

    interface ItemClickListener {
        fun onItemClicked(item: RestaurantModel)
    }
}