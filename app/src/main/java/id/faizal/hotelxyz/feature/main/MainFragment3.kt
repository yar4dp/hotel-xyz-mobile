package id.faizal.hotelxyz.feature.main

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.faizal.hotelxyz.R
import id.faizal.hotelxyz.feature.restaurant.RestaurantActivity
import kotlinx.android.synthetic.main.fragment_main_3.*

class MainFragment3 : Fragment() {

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_main_3, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        restaurantButton.setOnClickListener {
            startActivity(Intent(context, RestaurantActivity::class.java))
        }
    }

}