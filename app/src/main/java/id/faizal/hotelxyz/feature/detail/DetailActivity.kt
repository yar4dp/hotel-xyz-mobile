package id.faizal.hotelxyz.feature.detail

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.Html
import android.view.MenuItem
import android.view.View
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import id.faizal.hotelxyz.R
import id.faizal.hotelxyz.feature.authenticate.login.LoginActivity
import id.faizal.hotelxyz.feature.reservation.create.CreateReservationActivity
import id.faizal.hotelxyz.model.DetailModel
import id.faizal.hotelxyz.model.RoomModel
import id.faizal.hotelxyz.utilities.Customer
import id.faizal.hotelxyz.utilities.showSnackbar
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.content_detail.*


class DetailActivity : AppCompatActivity() {

    var detail : DetailModel? = null
    var room : RoomModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        setSupportActionBar(toolbar)

        val detail : DetailModel = intent.extras["detail"] as DetailModel
        this.detail = detail

        title = detail.name
        val description = detail.description.replace("\u2028", "")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            content.text = Html.fromHtml(description, Html.FROM_HTML_MODE_COMPACT)
        } else {
            content.text = Html.fromHtml(description)
        }

        toolbar_layout.background = null
        Picasso.with(this).load(detail.imageUrl).into(object : Target {

            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                // do nothing
            }

            override fun onBitmapFailed(errorDrawable: Drawable?) {
                // do nothing
            }

            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                showImage(bitmap!!)
            }

        })

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (intent.extras["room"] != null) {
            val room = intent.extras["room"] as RoomModel
            this.room = room
            app_bar.layoutParams.height = (150 * resources.getDisplayMetrics().density + 0.5f).toInt()
            fab.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_create_white_24dp))
            fab.setOnClickListener {
                if (Customer.hasBeenLoggedIn(this@DetailActivity)) {
                    showReservationActivity(room)
                } else {
                    showLoginForm()
                }
            }
        } else {
            if (detail.url != null) {
                fab.setOnClickListener {
                    val url = "http://drive.google.com/viewerng/viewer?embedded=true&url=" + detail.url
                    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
                }
                fab.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_restaurant_menu_white_24dp))
            } else {
                fab.visibility = View.GONE
            }
        }

        if (intent.extras["message"] != null && Customer.hasBeenLoggedIn(this)) {
            showSnackbar(window.decorView, "Welcome back " + Customer.profile?.fullName() + "!")
        }
    }

    private fun showLoginForm() {
        val loginIntent = Intent(this, LoginActivity::class.java)
        loginIntent.putExtra("previousIntent", intent)
        startActivity(loginIntent)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    fun showImage(bitmap: Bitmap) {
        toolbar_layout.background = BitmapDrawable(resources, bitmap)
    }

    private fun showReservationActivity(room: RoomModel) {
        val intent = Intent(this, CreateReservationActivity::class.java)
        intent.putExtra("room", room)
        startActivity(intent)
    }
}
