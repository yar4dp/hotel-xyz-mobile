package id.faizal.hotelxyz.feature.reservation.create

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import id.faizal.hotelxyz.R
import id.faizal.hotelxyz.feature.reservation.list.ListReservationActivity
import id.faizal.hotelxyz.model.EstimatedRateModel
import id.faizal.hotelxyz.model.ReservationModel
import id.faizal.hotelxyz.model.ResponseModel
import id.faizal.hotelxyz.model.RoomModel
import id.faizal.hotelxyz.network.apiService
import id.faizal.hotelxyz.utilities.*
import kotlinx.android.synthetic.main.activity_create_reservation.*
import kotlinx.android.synthetic.main.content_create_reservation.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class CreateReservationActivity : AppCompatActivity(), Callback<ResponseModel<EstimatedRateModel>> {

    var room : RoomModel? = null
    var checkIn = Calendar.getInstance()
    var checkOut = Calendar.getInstance()
    var simulatorMode = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_reservation)
        setSupportActionBar(toolbar)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        title = "Create Reservation"

        room = intent.extras["room"] as RoomModel

        inputRoomType.setText(room?.name)

        checkboxSelfReservation.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                selfReservation()
            } else {
                clearGuest()
            }
        }

        checkboxSelfReservation.isChecked = true

        fab.setOnClickListener {
            if (isReservationValid()) {
                simulateReservation()
            }
        }

        checkIn.add(Calendar.DAY_OF_MONTH, 1)
        inputCheckInDate.setText(formatCalendar(checkIn))
        inputCheckInDate.setOnClickListener {
            showDatePicker(this, checkIn, DatePickerDialog.OnDateSetListener { p0, y, m, d ->
                setCheckInDate(y, m, d)
                setCheckOutDate(y, m, d + 1)
            })
        }

        checkOut.add(Calendar.DAY_OF_MONTH, 2)
        inputCheckOutDate.setText(formatCalendar(checkOut))
        inputCheckOutDate.setOnClickListener {
            showDatePicker(this, checkOut, DatePickerDialog.OnDateSetListener { p0, y, m, d ->
                setCheckOutDate(y, m, d)
            })
        }

        checkIn.set(Calendar.HOUR_OF_DAY, 14)
        checkIn.set(Calendar.MINUTE, 0)
        inputCheckInTime.setText(formatCalendar(checkIn, "hh:mm a"))
        inputCheckInTime.setOnClickListener {
            showTimePicker(this, checkIn, TimePickerDialog.OnTimeSetListener { p0, h, m ->
                setCheckInTime(h, m)
            })
        }

        checkOut.set(Calendar.HOUR_OF_DAY, 9)
        checkOut.set(Calendar.MINUTE, 0)
        inputCheckOutTime.setText(formatCalendar(checkOut, "hh:mm a"))
        inputCheckOutTime.setOnClickListener {
            showTimePicker(this, checkOut, TimePickerDialog.OnTimeSetListener { p0, h, m ->
                setCheckOutTime(h, m)
            })
        }

        buttonReservation.setOnClickListener {
            reservation()
        }

        inputPerson.setText("2")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (simulatorMode) {
            exitSimulateReservation()
        } else {
            super.onBackPressed()
        }
    }

    private fun selfReservation() {
        val customerProfile = Customer.profile

        inputFirstName.setText(customerProfile?.firstName)
        inputFirstName.isFocusable = false

        inputLastName.setText(customerProfile?.lastName)
        inputLastName.isFocusable = false

        inputAddress.setText(customerProfile?.address)
        inputAddress.isFocusable = false

        inputPhoneNumber.setText(customerProfile?.phoneNumber)
        inputPhoneNumber.isFocusable = false
    }

    private fun clearGuest() {
        inputFirstName.setText("")
        inputFirstName.isFocusable = true
        inputFirstName.isFocusableInTouchMode = true

        inputLastName.setText("")
        inputLastName.isFocusable = true
        inputLastName.isFocusableInTouchMode = true

        inputAddress.setText("")
        inputAddress.isFocusable = true
        inputAddress.isFocusableInTouchMode = true

        inputPhoneNumber.setText("")
        inputPhoneNumber.isFocusable = true
        inputPhoneNumber.isFocusableInTouchMode = true

        inputFirstName.requestFocus()
    }

    private fun setCheckInDate(y: Int, m: Int, d:Int) {
        checkIn.set(Calendar.YEAR, y)
        checkIn.set(Calendar.MONTH, m)
        checkIn.set(Calendar.DAY_OF_MONTH, d)
        inputCheckInDate.setText(formatCalendar(checkIn))
    }
    
    private fun setCheckInTime(h: Int, m: Int) {
        checkIn.set(Calendar.HOUR_OF_DAY, h)
        checkIn.set(Calendar.MINUTE, m)
        inputCheckInTime.setText(formatCalendar(checkIn, "hh:mm a"))
    }

    private fun setCheckOutDate(y: Int, m: Int, d:Int) {
        checkOut.set(Calendar.YEAR, y)
        checkOut.set(Calendar.MONTH, m)
        checkOut.set(Calendar.DAY_OF_MONTH, d)
        inputCheckOutDate.setText(formatCalendar(checkOut))
    }
    
    private fun setCheckOutTime(h: Int, m: Int) {
        checkOut.set(Calendar.HOUR_OF_DAY, h)
        checkOut.set(Calendar.MINUTE, m)
        inputCheckOutTime.setText(formatCalendar(checkOut, "hh:mm a"))
    }

    private fun isReservationValid() : Boolean {
        if (inputFirstName.text.toString().isEmpty()) {
            showSnackbar(window.decorView, "Please input guest's first name.")
            return false
        }
        if (inputLastName.text.toString().isEmpty()) {
            showSnackbar(window.decorView, "Please input guest's last name.")
            return false
        }
        if (inputAddress.text.toString().isEmpty()) {
            showSnackbar(window.decorView, "Please input guest's address.")
            return false
        }
        if (inputPhoneNumber.text.toString().isEmpty()) {
            showSnackbar(window.decorView, "Please input guest's phone number")
            return false
        }
        if (inputPerson.text.toString().isEmpty()) {
            showSnackbar(window.decorView, "Please input how many person will book the room.")
            return false
        }
        val person = inputPerson.text.toString().toInt()
        if (person > 4) {
            showSnackbar(window.decorView, "Max 5 guests permitted.")
        }
        return true
    }

    private fun simulateReservation() {
        apiService.estimateRate(
                room!!.id,
                inputPerson.text.toString(),
                formatCalendar(checkIn, "yyyy-MM-dd HH:mm:ss"),
                formatCalendar(checkOut, "yyyy-MM-dd HH:mm:ss")
        ).enqueue(this)
    }

    private fun enterSimulateReservation() {
        inputFirstName.isEnabled = false
        inputFirstName.setTextColor(Color.BLACK)

        inputLastName.isEnabled = false
        inputLastName.setTextColor(Color.BLACK)

        inputAddress.isEnabled = false
        inputAddress.setTextColor(Color.BLACK)

        inputPhoneNumber.isEnabled = false
        inputPhoneNumber.setTextColor(Color.BLACK)

        inputPerson.isEnabled = false
        inputPerson.setTextColor(Color.BLACK)

        checkboxSelfReservation.isEnabled = false

        inputRoomType.isEnabled = false
        inputRoomType.setTextColor(Color.BLACK)

        inputAdditionalRequest.isEnabled = false
        inputAdditionalRequest.setTextColor(Color.BLACK)

        inputCheckInDate.isEnabled = false
        inputCheckInDate.setTextColor(Color.BLACK)

        inputCheckInTime.isEnabled = false
        inputCheckInTime.setTextColor(Color.BLACK)

        inputCheckOutDate.isEnabled = false
        inputCheckOutDate.setTextColor(Color.BLACK)

        inputCheckOutTime.isEnabled = false
        inputCheckOutTime.setTextColor(Color.BLACK)

        fab.visibility = View.GONE
        extraSpaceForFab.visibility = View.GONE

        priceCard.visibility = View.VISIBLE

        simulatorMode = true
        scrollView.post(Runnable {
            scrollView.fullScroll(View.FOCUS_DOWN)
        })
    }

    private fun exitSimulateReservation() {
        inputFirstName.isEnabled = true
        inputLastName.isEnabled = true
        inputAddress.isEnabled = true
        inputPhoneNumber.isEnabled = true
        inputPerson.isEnabled = true
        checkboxSelfReservation.isEnabled = true
        inputRoomType.isEnabled = true
        inputAdditionalRequest.isEnabled = true
        inputCheckInDate.isEnabled = true
        inputCheckInTime.isEnabled = true
        inputCheckOutDate.isEnabled = true
        inputCheckOutTime.isEnabled = true
        fab.visibility = View.VISIBLE
        extraSpaceForFab.visibility = View.VISIBLE
        priceCard.visibility = View.GONE
        simulatorMode = false
        scrollView.post {
            scrollView.fullScroll(View.FOCUS_UP)
        }
    }

    private fun reservation() {
        apiService.createReservation(
                Customer.token(this)!!,
                inputFirstName.text.toString(),
                inputLastName.text.toString(),
                inputAddress.text.toString(),
                inputPhoneNumber.text.toString(),
                inputPerson.text.toString(),
                room!!.id,
                formatCalendar(checkIn, "yyyy-MM-dd HH:mm:ss"),
                formatCalendar(checkOut, "yyyy-MM-dd HH:mm:ss"),
                inputAdditionalRequest.text.toString()
        ).enqueue(object: Callback<ResponseModel<ReservationModel>> {

            override fun onResponse(call: Call<ResponseModel<ReservationModel>>?, response: Response<ResponseModel<ReservationModel>>) {
                if (response.body()?.status == "200") {
                    reservationSuccess(response.body()?.data!!)
                } else {
                    reservationFailed(response.body()?.message!!)
                }
            }

            override fun onFailure(call: Call<ResponseModel<ReservationModel>>?, t: Throwable) {
                reservationFailed(t.localizedMessage)
            }

        })
    }

    override fun onFailure(call: Call<ResponseModel<EstimatedRateModel>>?, t: Throwable) {
        showSnackbar(window.decorView, t.localizedMessage)
    }

    override fun onResponse(call: Call<ResponseModel<EstimatedRateModel>>?, response: Response<ResponseModel<EstimatedRateModel>>) {
        if (response.body()?.status == "200") {
            inputEstimatedRate.setText(formatCurrency(response.body()?.data?.value!!))
            enterSimulateReservation()
        } else {
            showSnackbar(window.decorView, response.body()!!.message)
        }
    }

    private fun reservationSuccess(reservation: ReservationModel) {
        AlertDialog.Builder(this)
                .setTitle("Your reservation has been recorded")
                .setMessage("Thanks for your reservation. Please wait our employee to check if your reservation can be fulfilled.")
                .setPositiveButton("OK", DialogInterface.OnClickListener { dialogInterface: DialogInterface, i: Int ->
                    showReservationList()
                })
                .show()
    }

    private fun reservationFailed(reason: String) {
        showSnackbar(window.decorView, reason)
    }

    private fun showReservationList() {
        startActivity(Intent(this, ListReservationActivity::class.java))
    }

}
