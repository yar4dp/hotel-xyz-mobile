package id.faizal.hotelxyz.feature.reservation.list

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import android.view.View
import id.faizal.hotelxyz.R
import id.faizal.hotelxyz.feature.main.MainActivity
import id.faizal.hotelxyz.model.PaginationModel
import id.faizal.hotelxyz.model.ReservationModel
import id.faizal.hotelxyz.model.ResponseModel
import id.faizal.hotelxyz.network.apiService
import id.faizal.hotelxyz.utilities.Customer
import id.faizal.hotelxyz.utilities.showSnackbar
import kotlinx.android.synthetic.main.activity_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ListReservationActivity: AppCompatActivity(), Callback<ResponseModel<PaginationModel<ReservationModel>>> {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        title = "Your Reservation"

        recyclerViewLayout.visibility = View.GONE

        apiService.listReservation(Customer.token(this)!!).enqueue(this)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        startActivity(Intent(this, MainActivity::class.java))
        finishAffinity()
    }

    override fun onResponse(call: Call<ResponseModel<PaginationModel<ReservationModel>>>?, response: Response<ResponseModel<PaginationModel<ReservationModel>>>) {
        if (response.body()?.status == "200") {
            showReservationList(response.body()?.data!!.data)
        } else {
            showSnackbar(window.decorView, response.body()!!.message)
        }
    }

    override fun onFailure(call: Call<ResponseModel<PaginationModel<ReservationModel>>>?, t: Throwable) {
        showSnackbar(window.decorView, t.localizedMessage)
    }

    fun showReservationList(list: List<ReservationModel>) {
        progressBarLayout.visibility = View.GONE
        recyclerViewLayout.visibility = View.VISIBLE
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = ListReservationAdapter(this, list)
        recyclerView.isNestedScrollingEnabled = false
    }

}