package id.faizal.hotelxyz.utilities

import android.content.Context
import id.faizal.hotelxyz.model.CustomerModel
import id.faizal.hotelxyz.model.ResponseModel
import id.faizal.hotelxyz.model.TokenModel
import id.faizal.hotelxyz.network.apiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object Customer : Callback<ResponseModel<CustomerModel>> {

    var profile: CustomerModel? = null
    var listener: GetProfileInterface? = null

    fun token(fromContext: Context) : String? {
        val sharedPreferences = fromContext.getSharedPreferences(sharedPreferencesKey, Context.MODE_PRIVATE)
        return sharedPreferences.getString("token", null)
    }

    fun hasToken(fromContext: Context) : Boolean {
        if (token(fromContext) != null) {
            return true
        }
        return false
    }

    fun refreshProfile(token: String, listener: GetProfileInterface) {
        this.listener = listener
        refreshProfile(token)
    }

    fun hasBeenLoggedIn(fromContext: Context) : Boolean {
        if (token(fromContext) != null && profile != null) {
            return true
        }
        return false
    }

    fun login(fromContext: Context, withToken: TokenModel, listener: GetProfileInterface) {
        val editor = fromContext.getSharedPreferences(sharedPreferencesKey, Context.MODE_PRIVATE).edit()
        editor.putString("token", withToken.token)
        editor.apply()

        refreshProfile(withToken.token, listener)
    }

    fun logout(fromContext: Context) {
        val editor = fromContext.getSharedPreferences(sharedPreferencesKey, Context.MODE_PRIVATE).edit()
        editor.remove("token")
        editor.apply()

        profile = null
    }

    private fun refreshProfile(token: String) {
        apiService.customerProfile(token).enqueue(this)
    }

    override fun onFailure(call: Call<ResponseModel<CustomerModel>>?, t: Throwable) {
        listener?.profileFailedToRetrievedBecause(t.localizedMessage)
        listener = null
        t.printStackTrace()
    }

    override fun onResponse(call: Call<ResponseModel<CustomerModel>>?, response: Response<ResponseModel<CustomerModel>>?) {
        if (response?.body()?.status == "200") {
            profile = response.body()?.data
            listener?.profileSuccessRetrieved()
        } else {
            listener?.profileFailedToRetrievedBecause(response?.body()?.message!!)
        }
        listener = null
    }

    interface GetProfileInterface {
        fun profileSuccessRetrieved()
        fun profileFailedToRetrievedBecause(message: String)
    }

}