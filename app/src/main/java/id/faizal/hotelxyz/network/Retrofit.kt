package id.faizal.hotelxyz.network

import id.faizal.hotelxyz.model.*
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

val serviceDateFormatPattern = "yyyy-MM-dd"

val baseURL: String = "http://faizal-hotel-xyz.96.lt/api/"

val retrofit: Retrofit =
        Retrofit.Builder()
                .baseUrl(baseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()


val apiService: ApiService = retrofit.create(ApiService::class.java)

interface ApiService {

    @GET("restaurant")
    @Headers("x-api-key: DlSOCbO3MAdFbmNZVGSLivgnIY0rpR2I")
    fun listRestaurant() : Call<ResponseModel<List<RestaurantModel>>>

    @GET("facility")
    @Headers("x-api-key: DlSOCbO3MAdFbmNZVGSLivgnIY0rpR2I")
    fun listFacility() : Call<ResponseModel<List<FacilityModel>>>

    @GET("room")
    @Headers("x-api-key: DlSOCbO3MAdFbmNZVGSLivgnIY0rpR2I")
    fun listRoom() : Call<ResponseModel<List<RoomModel>>>

    @POST("customer/register")
    @FormUrlEncoded
    @Headers("x-api-key: DlSOCbO3MAdFbmNZVGSLivgnIY0rpR2I")
    fun customerRegistration(

            @Field("email")
            email: String,

            @Field("password")
            password: String,

            @Field("first_name")
            firstName: String,

            @Field("last_name")
            lastName: String,

            @Field("birth_date")
            birthday: String,

            @Field("address")
            address: String,

            @Field("phone_number")
            phoneNumber: String

            ) : Call<ResponseModel<TokenModel>>


    @POST("customer/profile")
    @Headers("x-api-key: DlSOCbO3MAdFbmNZVGSLivgnIY0rpR2I")
    fun customerProfile(

            @Header("Authorization")
            token: String

    ) : Call<ResponseModel<CustomerModel>>


    @POST("customer/login")
    @FormUrlEncoded
    @Headers("x-api-key: DlSOCbO3MAdFbmNZVGSLivgnIY0rpR2I")
    fun customerLogin(

            @Field("email")
            email: String,

            @Field("password")
            password: String

    ) : Call<ResponseModel<TokenModel>>


    @POST("estimated-rate")
    @FormUrlEncoded
    @Headers("x-api-key: DlSOCbO3MAdFbmNZVGSLivgnIY0rpR2I")
    fun estimateRate(

            @Field("room_id")
            roomId: String,

            @Field("total_person")
            totalPerson: String,

            @Field("check_in")
            checkIn: String,

            @Field("check_out")
            checkOut: String

    ) : Call<ResponseModel<EstimatedRateModel>>


    @POST("customer/reservation/create")
    @FormUrlEncoded
    @Headers("x-api-key: DlSOCbO3MAdFbmNZVGSLivgnIY0rpR2I")
    fun createReservation (

            @Header("Authorization")
            token: String,

            @Field("guest_first_name")
            guestFirstName: String,

            @Field("guest_last_name")
            guestLastName: String,

            @Field("guest_address")
            guestAddress: String,

            @Field("guest_phone_number")
            guestPhoneNumber: String,

            @Field("total_person")
            totalPerson: String,

            @Field("room_id")
            roomId: String,

            @Field("check_in")
            checkIn: String,

            @Field("check_out")
            checkOut: String,

            @Field("additional_request")
            additionalRequest: String

    ) : Call<ResponseModel<ReservationModel>>


    @POST("customer/reservation/list")
    @Headers("x-api-key: DlSOCbO3MAdFbmNZVGSLivgnIY0rpR2I")
    fun listReservation(

            @Header("Authorization")
            token: String

    ) : Call<ResponseModel<PaginationModel<ReservationModel>>>

}