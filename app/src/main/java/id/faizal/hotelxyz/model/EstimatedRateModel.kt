package id.faizal.hotelxyz.model

import com.google.gson.annotations.SerializedName

data class EstimatedRateModel (

        @SerializedName("estimated_rate")
        val value: Double

)