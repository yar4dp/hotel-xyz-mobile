package id.faizal.hotelxyz.model

import java.io.Serializable
import java.text.DecimalFormat

data class DetailModel (
        val name: String,
        val description: String,
        val imageUrl: String,
        val url: String?
) : Serializable {

    companion object {

        fun fromFacility(facility: FacilityModel) : DetailModel {
            return DetailModel(facility.name, facility.description, facility.imageUrl, null)
        }

        fun fromRestaurant(restaurant: RestaurantModel) : DetailModel {
            val description = restaurant.description +
                    "<p><b>House of Operation</b><br/>" + restaurant.houseOfOperation +
                    "</p><p><b>Dress Code</b><br/>" + restaurant.dressCode +
                    "</p><p><b>How to Reserve</b><br/>" + restaurant.howToReserve + "</p>"
            return DetailModel(restaurant.name, description, restaurant.imageUrl, restaurant.menuListUrl)
        }

        fun fromRoom(room: RoomModel) : DetailModel {
            val decimalFormat = DecimalFormat("#,###")
            val rate = "IDR " + decimalFormat.format(room.rate * 1000)
            val description = room.description +
                    "<p><b>Size</b><br/>" + room.size +
                    "<p><b>Bed Type</b><br/>" + room.bedType +
                    "<p><b>View</b><br/>" + room.view +
                    "<p><b>Estimated Rate</b><br/>" + rate.removeSuffix(".0") + " / night"
            return DetailModel(room.name, description, room.imageUrl, null)
        }

    }

}