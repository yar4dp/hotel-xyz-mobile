package id.faizal.hotelxyz.model

import com.google.gson.annotations.SerializedName

data class CustomerModel (

        @SerializedName("first_name")
        val firstName: String,

        @SerializedName("last_name")
        val lastName: String,

        @SerializedName("birth_date")
        val birthday: String,

        @SerializedName("address")
        val address: String,

        @SerializedName("phone_number")
        val phoneNumber: String,

        @SerializedName("user")
        val user: UserModel
) {

    fun fullName() : String = firstName + " " + lastName

}