package id.faizal.hotelxyz.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class RoomModel (

        @SerializedName("id")
        val id: String,

        @SerializedName("name")
        val name: String,

        @SerializedName("description")
        val description: String,

        @SerializedName("size")
        val size: String,

        @SerializedName("bed_type")
        val bedType: String,

        @SerializedName("view")
        val view: String,

        @SerializedName("rate")
        val rate: Double,

        @SerializedName("image_url")
        val imageUrl: String

) : Serializable