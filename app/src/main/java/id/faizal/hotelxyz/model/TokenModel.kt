package id.faizal.hotelxyz.model

data class TokenModel (
        val token: String
)