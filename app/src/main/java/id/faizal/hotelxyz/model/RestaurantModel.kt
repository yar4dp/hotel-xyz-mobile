package id.faizal.hotelxyz.model

import com.google.gson.annotations.SerializedName

data class RestaurantModel(

        @SerializedName("name")
        val name: String,

        @SerializedName("description")
        val description: String,

        @SerializedName("house_of_operation")
        val houseOfOperation: String,

        @SerializedName("dress_code")
        val dressCode: String,

        @SerializedName("how_to_reserve")
        val howToReserve: String,

        @SerializedName("menu_list_url")
        val menuListUrl: String,

        @SerializedName("image_url")
        val imageUrl: String
)